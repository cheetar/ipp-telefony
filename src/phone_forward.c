/** @file
 * Implementacja klasy przechowującej przekierowania numerów telefonicznych
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 13.05.2018
 */

#include <string.h>
#include <assert.h>
#include <ctype.h>
#include "phone_forward.h"
#include "utils.h"

/** @brief Liczba cyfr
 *  Liczba cyfr, z której składają się numery.
 */
#define MAX_DIGITS 10

/** @brief Struktura przechowująca przekierowania.
 * Przekierowania numerów są trzymane w drzewie prefiksowym (trie). W każdym
 * wierzchołku znajduje się wskaźnik na napis reprezentujący przekierowanie
 * bądź NULL jeśli w danym wierzchołku nie ma przekierowania. Każda krawędź
 * jest etykietowana cyfrą z zakresu {0..9}. Konkatynacja wszystkich liter na
 * krawędziach od korzenia do danego wierzchołka reprezentuje napis, który jest
 * przekierowywany, a @p forward wskazuje na napis reprezentujący
 * przekierowanie.
 */
struct PhoneForward {
/**
 *  Wskaźnik na napis, który reprezentuje przekierownanie.
 */
  const char *forward;

/**
 *  Tablica wskaźników na synów danego wierzchołka.
 */
  Forwards *sons[MAX_DIGITS];
};

/** @brief Struktura przechowująca numery.
 * Numery są trzymane w drzewie BST. W każdym wierzchołku jest wskaźnik na
 * napis reprezentujący numer, wskaźniki na lewe oraz prawe poddrzewo, oraz
 * liczba elementów w lewym oraz prawym poddrzewie. Numery w drzewie są
 * posortowane leksykograficznie. Dla każdego wierzchołka, w jego lewym
 * poddrzewie znajdują się numery mniejsze leksykograficznie, a w prawym
 * większe leksykograficznie od numeru w danym wierzchołku.
 */
struct PhoneNumbers {
  char *number;
  int sizeleft, sizeright;
  Numbers *left, *right;
};

/** @brief Porównuje dwa numery w kolejności leksykograficznej.
 * Sprawdza, który z dwóch napisów jest pierwszy w kolejności leksykograficznej.
 * Zwraca @p false, jeśli napisy są takie same.
 * @param[in] a – wskaźnik na pierwszy napis reprezentujący numer;
 * @param[in] b – wskaźnik na drugi napis reprezentujący numer.
 * @return Wartość @p true, jeśli pierwszy napis jest pierwszy w kolejności
 *         leksykograficznej.
 *         Wartość @p false, jeśli drugi napis jest pierwszy w kolejności
 *         leksykograficznej lub gdy oba napisy są sobie równe.
 */
// Returns true if num1 is first in lexygrophical order
static bool compareNum(char const *a, char const *b) {
  int len = min(strlen(a), strlen(b));
  for (int i=0; i < len; i++) {
    if (a[i] < b[i]) {
      return true;
    }
    else if (a[i] > b[i]) {
      return false;
    }
  }
  return (strlen(a) < strlen(b));
}

/** @brief Sprawdza czy jeden napis jest prefiksem drugiego.
 * Sprawda, czy @p prefix jest prefiksem @p text.
 * @param[in] text   – wskaźnik na napis przechowujący tekst;
 * @param[in] prefix – wskaźnik na napis reprezentujący prefiks.
 * @return Wartość @p true, jeśli @p prefix jest prefiksem @p text.
           Wartość @p false, jeśli @p prefix nie jest prefiksem @p text lub
           @p prefix jest pusty lub @p prefix jest dłuższy niż @p text.
 */
static bool isPrefix(char const *text, char const *prefix) {
  int lenprefix = strlen(prefix);
  int lentext = strlen(text);

  if (lentext < lenprefix || lenprefix == 0) {
    return false;
  }
  else {
    return strncmp(text, prefix, lenprefix) == 0;
  }
}

/** @brief Łączy dwa napisy.
 * Alokuje nowy napis, którego wartością jest zkonkatynowane dwa napisy @p a
 * oraz @p b z odpowiednimi offsetami. @p offseta oznacza, ile pierwszych znaków
 * @p a należy pominąć, analogicznie @p offsetb. Alokuje nowy napis, który musi
 * być zwolniony za pomocą funkcji free().
 * Np. concatinateStrings("abcd", 0, "1234", 2) zwróci wskaźnik na napis
 * "abcd34".
 * @param[in] a       – wskaźnik na pierwszy napis;
 * @param[in] offseta – liczba reprezentująca offset(ile znaków pominąć)
                        pierwszego napisu;
 * @param[in] b       – wskaźnik na drugi napis;
 * @param[in] offsetb – liczba reprezentująca offset(ile znaków pominąć)
                        drugiego napisu.
 * @return Wskaźnik na napis będący złączeniem napisów a i b z odpowiednimi
           offsetami.
 */
static char * concatinateStrings(char const *a, int offseta, char const *b, int offsetb) {
  int lena = strlen(a);
  int lenb = strlen(b);
  assert(lena >= offseta && lenb >= offsetb);

  char *result = malloc(lena - offseta + lenb - offsetb + 1);
  assert(result != NULL);

  strcpy(result, a + offseta);
  strcat(result, b + offsetb);
  return result;
}

/** @brief Tworzy nową strukturę.
 * Tworzy nową strukturę niezawierającą żadnych numerów.
 * @return Wskaźnik na utworzoną strukturę lub NULL, gdy nie udało się
 *         zaalokować pamięci.
 */
static Numbers * phnumNew(void) {
  Numbers *pn = malloc(sizeof(Numbers));
  if (pn == NULL) {
    return NULL;
  }

  pn->number = NULL;
  pn->left = NULL;
  pn->right = NULL;
  pn->sizeleft = 0;
  pn->sizeright = 0;

  return pn;
}

/** @brief Dodaje nowy numer.
 * Dodaje nowy numer do strukury @p pn. Numer nie jest dodawany do struktury,
 * jeśli istnieje w niej taki sam numer.
 * @param[in, out] pn   – wskaźnik na strukturę przechowującą numery;
 * @param[in] pnum – wskaźnik na napis reprezentujący numer.
 * @return Wartość @p true, jeśli numer został dodany.
 *         Wartość @p false, jeśli numer nie został dodany (tzn. podany numer
 *         już był w strukturze).
 */
static bool phnumAdd(Numbers *pn, char const *pnum) {
  if (pn->number == NULL) {
    pn->number = (char *)pnum;
    return true;
  }
  else {
    if (strcmp(pnum, pn->number) != 0) {
      if (compareNum(pnum, pn->number)) {
        // Dodaj do lewego poddrzewa.
        if (pn->left == NULL) {
          pn->left = phnumNew();
        }
        pn->sizeleft++;
        return phnumAdd(pn->left, pnum);
      }
      else {
        // Dodaj do prawego poddrzewa.
        if (pn->right == NULL) {
          pn->right = phnumNew();
        }
        pn->sizeright++;
        return phnumAdd(pn->right, pnum);
      }
    }
    else {
      return false;
    }
  }
}

void phnumDelete(Numbers const *pn) {
  if (pn != NULL) {
    if (pn->number != NULL) {
      free((char *)(pn->number));
    }
    phnumDelete(pn->left);
    phnumDelete(pn->right);
  }
  // TODO free(pn);
}

char const * phnumGet(Numbers const *pn, size_t idx) {
  if (pn == NULL || pn->number == NULL) {
    return NULL;
  }
  if (pn->sizeleft == (int)idx) {
    return pn->number;
  }
  else if (pn->sizeleft > (int)idx) {
    // Szukany numer jest w lewym poddrzewie.
    return phnumGet(pn->left, idx);
  }
  else if (pn->sizeleft + pn->sizeright >= (int)idx) {
    // Szukany numer jest w prawym poddrzewie.
    return phnumGet(pn->right, idx - pn->sizeleft - 1);
  }
  else {
    // Indeks przekracza liczbę elementów w drzewie.
    return NULL;
  }
}

Forwards * phfwdNew(void) {
  Forwards *pf = malloc(sizeof(Forwards));
  if (pf == NULL) {
    return NULL;
  }

  pf->forward = NULL;
  for (int i = 0; i < MAX_DIGITS; i++) {
    pf->sons[i] = NULL;
  }

  return pf;
}

void phfwdDelete(Forwards *pf) {
  if (pf != NULL) {
    // Rekurencyjnie usuń wszystkich synów.
    for (int i=0; i < MAX_DIGITS; i++) {
      phfwdDelete(pf->sons[i]);
      pf->sons[i] = NULL;
    }

    // Usuń wierzchołek.
    if (pf->forward != NULL) {
      free((char *)(pf->forward));
    }
    free(pf);
  }
}

bool phfwdAdd(Forwards *pf, char const *num1, char const *num2) {
  if (!isNumValid(num1) || !isNumValid(num2)) {
    return false;
  }
  if (strlen(num1) == strlen(num2) && strncmp(num1, num2, strlen(num1)) == 0) {
    return false;
  }

  int n = strlen(num1);
  int k = strlen(num2);

  char *fwd = malloc(k + 1);
  if (fwd == NULL) {
    return false;
  }
  strcpy(fwd, num2);

  int i = 0;

  while(i < (n-1)) {
    int number = digitToInt(num1[i]);

    if (pf->sons[number] == NULL) {
      pf->sons[number] = phfwdNew();
    }
    pf = pf->sons[number];

    i++;
  }

  int number = digitToInt(num1[i]);
  if (pf->sons[number] == NULL){
    pf->sons[number] = phfwdNew();
  }
  pf->sons[number]->forward = fwd;

  return true;
}

void phfwdRemove(Forwards *pf, char const *num) {
  if (isNumValid(num)) {
    bool delete = true;
    int n = strlen(num);
    int i = 0;

    while((i < (n-1)) && delete) {
      int number = digitToInt(num[i]);

      if (pf->sons[number] == NULL) {
        delete = false;
      }
      else {
        pf = pf->sons[number];
      }

      i++;
    }

    if (delete) {
      int number = digitToInt(num[i]);
      phfwdDelete(pf->sons[number]);
      pf->sons[number] = NULL;
    }

  }
}

Numbers const * phfwdGet(Forwards *pf, char const *num) {
  if (!isNumValid(num)) {
    return phnumNew();
  }

  int n = strlen(num);
  char *result = malloc(n + 1);
  assert(result != NULL);
  strcpy(result, num);

  for (int i=0; i < n; i++) {
    int number = digitToInt(num[i]);
    if (pf->sons[number] == NULL) {
      break;
    }
    else {
      pf = pf->sons[number];
      if (pf->forward != NULL) {
        free(result);
        result = concatinateStrings(pf->forward, 0, num, i + 1);
      }
    }
  }

  Numbers *pn = phnumNew();
  phnumAdd(pn, result);
  return pn;
}

/** @brief Rekurencyjnie wyznacza przekierowania numeru.
 * Jeżeli wierzchołek @p pf zawiera przekierowanie, które jest prefiksem numeru
 * @p num, to dodaje ten numer z odpowiednio zamienionym prefiksem do struktury
 * @p pn przechowującej numery. Funkcja ta jest wywoływana rekurencyjnie dla
 * każdego syna @p pf.
 * @param[in] pf     – wskaźnik na wierzchołek w strukturze przechowującej
 *                     przekierowania;
 * @param[in] pn     – wskaźnik na strukturę przechowującą numery;
 * @param[in] num    – wskaźnik na napis reprezentujący numer do przekierowania;
 * @param[in] prefix – pomocnicza zmienna reprezentująca skonkatynowane
 *                     wartości w wierzchołkach leżących na ścieżce od korzenia
 *                     do obecnego wierzchołka @p pf.
 */
static void phfwdReverseAux(Forwards *pf, Numbers *pn, char const *num, char *prefix) {
  if (pf->forward != NULL) {
    if (isPrefix(num, pf->forward)) {
      int k = strlen(pf->forward);
      phnumAdd(pn, concatinateStrings(prefix, 0, num, k));
    }
  }

  for (int i=0; i < MAX_DIGITS; i++) {
    if (pf->sons[i] != NULL) {
      char *digit = intToDigit(i);
      char *newprefix = concatinateStrings(prefix, 0, digit, 0);
      phfwdReverseAux(pf->sons[i], pn, num, newprefix);
      free(digit);
    }
  }

  free(prefix);
}

Numbers const * phfwdReverse(Forwards *pf, char const *num) {
  Numbers *pn = phnumNew();

  if (!isNumValid(num)) {
    return pn;
  }

  char *numcopy = malloc(strlen(num) + 1);
  assert(numcopy != NULL);
  strcpy(numcopy, num);
  phnumAdd(pn, numcopy);

  char *prefix = malloc(sizeof(char));
  assert(prefix != NULL);
  strcpy(prefix, "");

  phfwdReverseAux(pf, pn, num, prefix);

  return pn;
}
