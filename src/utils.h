/** @file
 * Interfejs klasy zawierającej funkcje pomocnicze.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>

/** @brief Sprawdza, czy dany znak jest cyfrą.
 * Zwraca true jeśli podany znak jest cyfrą.
 * @param[in] ch – wejściowy znak.
 * @return true jeśli podany znak jest cyfrą, false w przeciwnym wypadku.
 */
bool isDigit(char ch);

/** @brief Sprawdza czy numer jest poprawny.
 * Sprawdza, czy numer @p num jest poprawny. Numer jest poprawny, jeśli @p num
 * nie jest NULLem, nie jest pusty oraz każdy jego znak zawiera się w zbiore
 * {0..9}.
 * @param[in] num – wskaźnik na napis reprezentujący numer.
 * @return Wartość @p true, jeśli napis jest poprawny.
 *         Wartość @p false w przeciwym wypadku.
 */
bool isNumValid(char const *num);

/** @brief Kowertuje cyfrę char na inta.
 * Konwertuje cyfrę char @p ch na odpowiadającą jej wartość int.
 * @param[in] ch – cyfra do skonwertowania.
 * @return Wartosć int odpowiadająca cyfrze wejściowej.
 */
int digitToInt(char ch);

/** @brief Konwertuje cyfrę int na chara.
 * Konwertuje cyfrę jako int na cyfrę jako char. Alokuje nowy napis, który
 * musi być zwolniony za pomoą funkcji free(ch).
 * @param[in] x – cyfra zapisana w zmiennej typu int do skonwertowania.
 * @return Wskaźnik @p ch na zaalokowany napis.
 */
char * intToDigit(int x);

/** @brief Minimum dwóch liczb.
 * Wyznacza i zwraca minimum spośród dwóch liczb typu int.
 * @param[in] a – pierwsza wartość typu int;
 * @param[in] b – druga wartość typu int.
 * @return Mniejsza liczba spośród a i b.
 */
int min(int a, int b);

#endif /* __UTILS_H__ */
