/** @file
 * Implementacja klasy obsługującej wczytywanie ze standardowego wejścia.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "reader.h"
#include "utils.h"
#include <stdio.h>

#define BUFFER_INIT_SIZE 1024

static bool truncateComment(Buffer *b);
char* copyString(char *original, long long size);

struct BufferReader {
  char ch, last_ch;
  char *buffer;
  long long buffer_size, already_read, current_position;
};

struct CommandStruct {
  char operator;
  char *first_arg;
  char *second_arg;
  long long operator_place;
};

struct LexemStruct {
  bool is_operator, is_number, is_identifier;
  char operator;
  char *value;
  long long traceback_error;
};

Buffer * BufferNew(void){
  Buffer *b = malloc(sizeof(Buffer));
  if (b == NULL) {
    return NULL;
  }

  b->ch = '\0';
  b->last_ch = '\0';
  b->already_read = 1;
  b->current_position = 0;
  b->buffer_size = BUFFER_INIT_SIZE;

  char *buffer = malloc(sizeof(char) * (BUFFER_INIT_SIZE + 1));
  if(buffer == NULL){
    return NULL;
  }
  b->buffer = buffer;

  return b;
}

void BufferDelete(Buffer *b){
  if (b != NULL) {
    free(b->buffer);
    free(b);
  }
}

Command * CommandNew(void){
  Command *cmd = malloc(sizeof(Command));
  if (cmd == NULL) {
    return NULL;
  }

  cmd->operator = '\0';
  cmd->first_arg = cmd->second_arg = NULL;
  cmd->operator_place = -1;

  return cmd;
}

void CommandDelete(Command *cmd){
  if(cmd != NULL) {
    free(cmd->first_arg);
    free(cmd->second_arg);
    free(cmd);
  }
}

Lexem * LexemNew(void){
  Lexem *lexem = malloc(sizeof(Lexem));
  if (lexem == NULL) {
    return NULL;
  }

  lexem->is_operator = lexem->is_number = lexem->is_identifier = false;
  lexem->operator = '\0';
  lexem->value = NULL;
  lexem->traceback_error = -1;

  return lexem;
}

void LexemDelete(Lexem *lexem){
  if(lexem != NULL) {
    free(lexem->value);
    free(lexem);
  }
}

static void clearBuffer(Buffer *b){
  for(long long i=0; i < b->buffer_size; i++){
    b->buffer[i] = '\0';
  }

  b->already_read += b->current_position;
  b->current_position = 0;
}

// Returns false if reallocation failed, true otherwise.
static bool resizeBufferIfNecessary(Buffer *b){
  char *new_buffer = malloc(sizeof(char) * (b->buffer_size * 2 + 1));
  if(new_buffer == NULL){
    return false;
  }

  // Copy content of previous buffer to the new buffer.
  for(long long i=0; i < (2 * b->buffer_size + 1); i++){
    if(i < b->buffer_size + 1){
      new_buffer[i] = b->buffer[i];
    }
    else{
      new_buffer[i] = '\0';
    }
  }

  free(b->buffer);
  b->buffer = new_buffer;
  b->buffer_size *= 2;

  return true;
}

// Returns false if couldn't reallocate buffer or unexpected EOF, true otherwise.
static bool readChar(Buffer *b){

  // Update buffer last char.
  b->last_ch = b->ch;

  // Read next char from input.
  b->ch = getchar();

  if(b->ch == '$'){
    b->already_read++;
    if(!truncateComment(b)){
      return false;
    }

    return readChar(b);
  }
  else{
    b->buffer[b->current_position] = b->ch;
    b->current_position++;
  }

  return resizeBufferIfNecessary(b);
}

// Truncates comment, returns false if encountered unexpected EOF or unexcpected char.
static bool truncateComment(Buffer *b){
  assert(b->ch == '$');

  char ch = getchar();
  char last_ch;

  if(ch == '$'){
    // Two consequent $$.
    b->already_read++;
    b->ch = b->last_ch = '\0';
  }
  else if(ch == EOF){
    fprintf(stderr, "ERROR EOF\n");
    return false;
  }
  else{
    // Unexpected character.
    fprintf(stderr, "ERROR %lld\n", b->already_read+ b->current_position);
    return false;
  }

  b->ch = b->last_ch = '\0';
  while(ch != '$' && last_ch != '$'){
    last_ch = ch;
    ch = getchar();
    b->already_read++;

    if(ch == EOF){
      fprintf(stderr, "ERROR EOF\n");
      return false;
    }
  }

  b->ch = b->last_ch = '\0';

  return true;
}

// Returns false if memory allocation error, true otherwise.
static bool truncateWhiteSpaces(Buffer *b){
  char ch;
  assert(isspace(b->ch) || b->ch == NULL);

  while((ch = getchar()) && isspace(ch)){
    b->already_read++;
  }

  if(ch == '$'){
    b->ch = '$';
    if(!truncateComment(b)){
      return false;
    }

    return readChar(b);
  }
  else{
    clearBuffer(b);
    b->ch = ch;
    return true;
  }
}

// return NULL if error occured
static Lexem* readNumber(Buffer *b){
  assert(isDigit(b->ch));
  assert(b->current_position == 1);

  while(isDigit(b->ch)){
    if(!readChar(b)){
      return NULL;
    }
  }

  Lexem *lexem = LexemNew();
  lexem->is_number = true;
  lexem->value = copyString(b->buffer, b->current_position);
  lexem->traceback_error = b->already_read;

  clearBuffer(b);
  return lexem;
}

static Lexem* readIdentifier(Buffer *b){
  assert(isalpha(b->ch));
  assert(b->current_position == 1);

  while(isalpha(b->ch) || isDigit(b->ch)){
    if(!readChar(b)){
      return false;
    }
  }

  Lexem *lexem = LexemNew();
  lexem->is_identifier = true;
  lexem->value = copyString(b->buffer, b->current_position);
  lexem->traceback_error = b->already_read;

  clearBuffer(b);
  return lexem;
}

static Lexem* readOperator(Buffer *b){
  assert(b->ch == '>' || b->ch == '?' || b->ch == 'N' || b->ch == 'D' /* || b->ch == '@' */);
  assert(b->current_position == 1);

  Lexem *lexem = LexemNew();
  lexem->is_operator = true;
  lexem->traceback_error = b->already_read;

  char ch;

  if(b->ch == '>'){
    lexem->operator = '>';
  }
  else if(b->ch == '?'){
    lexem->operator = '?';
  }
  /*else if(b->ch == '@'){
    lexem->operator = '@';
  }*/
  else if(b->ch == 'N' || b->ch == 'D'){

    lexem->operator = b->ch;

    char second_char = 'E';
    char third_char;
    if(b->ch == 'N'){
      third_char = 'W';
    }
    else{
      third_char = 'L';
    }

    ch = getchar();
    b->already_read++;

    if(ch != second_char){
      if(ch == EOF){
        fprintf(stderr, "ERROR EOF\n");
      }
      else{
        fprintf(stderr, "ERROR %lld\n", b->already_read);
      }
      LexemDelete(lexem);
      return NULL;
    }

    ch = getchar();
    b->already_read++;

    if(ch != third_char){
      if(ch == EOF){
        fprintf(stderr, "ERROR EOF\n");
      }
      else{
        fprintf(stderr, "ERROR %lld\n", b->already_read);
      }
      LexemDelete(lexem);
      return NULL;
    }
  }

  clearBuffer(b);
  return lexem;
}

// Returns NULL if problems with allocating memory or unexpected EOF, returns empty lexem if expected EOF encountered
static Lexem* readLexem(Buffer *b){
  if(b->ch == '\0'){
    if(!readChar(b)){
      return NULL;
    }
  }

  // Check if ch is a whitespace i.e. ' ', '\t', '\n', '\v', '\f', '\r'.
  while(isspace(b->ch)){
    if(!truncateWhiteSpaces(b)){
      return NULL;
    }
  }

  if(isDigit(b->ch)){
    return readNumber(b);
  }
  else if(b->ch == 'N' || b->ch == 'D' || b->ch == '>' || b->ch == '?'){
    return readOperator(b);
  }
  else if(isalpha(b->ch)){
    return readIdentifier(b);
  }
  else if(b->ch == EOF){
    // Return empty lexem.
    return LexemNew();
  }
  else {
    fprintf(stderr, "ERROR %lld\n", b->already_read);
    return NULL;
  }
}

Command* readCommand(Buffer *b){

  Command *cmd = CommandNew();
  Lexem *lexem;

  lexem = readLexem(b);
  if(lexem->is_operator){
    cmd->operator_place = lexem->traceback_error;

    if(lexem->operator == 'N'){
      cmd->operator = 'N';
      LexemDelete(lexem);

      lexem = readLexem(b);
      if(lexem->is_identifier){
        cmd->first_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));
      }
      else{
        fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
        CommandDelete(cmd);
        cmd = NULL;
      }
    }
    else if(lexem->operator == 'D'){
      cmd->operator = 'D';
      LexemDelete(lexem);

      lexem = readLexem(b);
      if(lexem->is_number || lexem->is_identifier){
        cmd->first_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));
      }
      else{
        fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
        CommandDelete(cmd);
        cmd = NULL;
      }
    }
    else if(lexem->operator == '?' /*|| lexem->operator == '@'*/){
      LexemDelete(lexem);
      lexem = readLexem(b);
      if(lexem->is_number){
        cmd->second_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));
      }
      else{
        fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
        CommandDelete(cmd);
        cmd = NULL;
      }
    }
    else{
      fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
      CommandDelete(cmd);
      cmd = NULL;
    }
  }
  else if(lexem->is_number){
    cmd->first_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));

    LexemDelete(lexem);
    lexem = readLexem(b);
    if(lexem->operator == '?' || lexem->operator == '>'){
      if(lexem->operator == '?'){
        cmd->operator = '?';
        cmd->first_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));
      }
      else{
        // > command.
        LexemDelete(lexem);
        lexem = readLexem(b);
        if(lexem->is_number){
          cmd->second_arg = copyString(lexem->value, sizeof(lexem->value)/sizeof(char));
          cmd = NULL;
        }
        else{
          fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
          CommandDelete(cmd);
          cmd = NULL;
        }
      }
    }
    else{
      fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
      CommandDelete(cmd);
      cmd = NULL;
    }
  }
  else{
    fprintf(stderr, "ERROR %lld\n", lexem->traceback_error);
    CommandDelete(cmd);
    cmd = NULL;
  }

  LexemDelete(lexem);
  return cmd;
}
