/** @file
 * Interfejs klasy obsługującej wczytywanie ze standardowego wejścia.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#ifndef __READER_H__
#define __PHONE_FORWARD_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/**
 *  Skrócona nazwa na strukturę przechowującą bufor do wczytywania ze standardowego wejścia.
 */
typedef struct BufferReader Buffer;

/**
 *  Skrócona nazwa na strukturę przechowującą komendę do wykonania.
 */
typedef struct CommandStruct Command;

/**
 * Struktura przechowująca komendę do wykonania.
 */
struct CommandStruct;

/**
 * Struktura przechowująca bufor do wczytywania ze standardowego wejścia.
 */
struct BufferReader;

/**
 *  Skrócona nazwa na strukturę przechowującą lexem.
 */
typedef struct LexemStruct Lexem;

/**
 * Struktura przechowująca lexem.
 */
struct LexemStruct;

/** @brief Tworzy nową strukturę.
 * Tworzy nową strukturę bufora.
 * @return Wskaźnik na utworzoną strukturę lub NULL, gdy nie udało się
 *         zaalokować pamięci.
 */
Buffer * BufferNew(void);

/** @brief Usuwa strukturę.
 * Usuwa strukturę wskazywaną przez @p b. Nic nie robi, jeśli wskaźnik ten ma
 * wartość NULL.
 * @param[in] b – wskaźnik na usuwaną strukturę.
 */
void BufferDelete(Buffer *b);

/** @brief Tworzy nową strukturę.
 * Tworzy nową strukturę komendy do wykonania.
 * @return Wskaźnik na utworzoną strukturę lub NULL, gdy nie udało się
 *         zaalokować pamięci.
 */
Command * CommandNew(void);

/** @brief Usuwa strukturę.
 * Usuwa strukturę wskazywaną przez @p cmd. Nic nie robi, jeśli wskaźnik ten ma
 * wartość NULL.
 * @param[in] cmd – wskaźnik na usuwaną strukturę.
 */
void CommandDelete(Command *cmd);

/** @brief Tworzy nową strukturę.
 * Tworzy nową strukturę lexemu.
 * @return Wskaźnik na utworzoną strukturę lub NULL, gdy nie udało się
 *         zaalokować pamięci.
 */
Lexem * LexemNew(void);

/** @brief Usuwa strukturę.
 * Usuwa strukturę wskazywaną przez @p b. Nic nie robi, jeśli wskaźnik ten ma
 * wartość NULL.
 * @param[in] lexem – wskaźnik na usuwaną strukturę.
 */
void LexemDelete(Lexem *lexem);

/** @brief Wczytuje następną komendę ze standardowego wejścia.
 * Wczytuje następną komendę ze standardowego wejścia.
 * @param[in] b – wskaźnik na bufor używany przy wczytywaniu.
 * @return Wskaźnik na utworzoną strukturę komendy lub NULL, gdy nie udało się
 *         zaalokować pamięci bądź napotkano na błąd przy wczytywaniu.
 */
Command* readCommand(Buffer *b);

#endif /* __READER_H__ */
