/** @file
 * Implementacja klasy zawierającej funkcje pomocnicze.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>


bool isDigit(char ch) {
  if(isdigit(ch)){
    return true;
  }
  else{
    return false;
  }
}

bool isNumValid(char const *num) {
  if (num == NULL) {
    return false;
  }

  size_t n = strlen(num);
  if (n == 0) {
    return false;
  }

  for (size_t i=0; i < n; i++) {
    if (!isDigit(num[i])) {
      return false;
    }
  }

  return true;
}

int digitToInt(char ch) {
  assert(isDigit(ch));

  int x = ch - '0';
  return x;
}

char * intToDigit(int x) {
  assert(0 <= x && x <= 9);
  char *ch = malloc(2);

  ch[0] = (char)(x + '0');
  ch[1] = '\0';
  return ch;
}

int min(int a, int b) {
  return a < b ? a : b;
}

char* copyString(char *original, long long size){
  char *destination = malloc(sizeof(char) * (size + 1));
  for(long long i=0; i < size; i++){
    destination[i] = original[i];
  }

  destination[size] = '\0';

  return destination;
}

bool isEqual(char *s1, char *s2){
  return (strcmp(s1, s2) == 0);
}
