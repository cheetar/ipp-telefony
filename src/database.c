/** @file
 * Implementacja klasy obsługującej bazy przekierowań.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#include <string.h>
#include <ctype.h>
#include "phone_forward.h"
#include "database.h"

struct DatabaseStruct {
  char *name;
  Forwards *forwards;
};

Database * DatabaseNew(char *name){
  Database *db = malloc(sizeof(Database));
  if (db == NULL) {
    return NULL;
  }

  Forwards *fwd = phfwdNew();
  db->forwards = fwd;
  db->name = name;

  return db;
}

void DatabaseDelete(Database *db){
  if (db != NULL) {
    phfwdDelete(db->forwards);
    free(db->name);
    free(db);
  }
}
