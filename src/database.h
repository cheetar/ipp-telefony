/** @file
 * Interfejs klasy baz przekierowań.
 *
 * @author Mateusz Sieniawski <ms394734@mimuw.edu.pl>
 * @date 17.08.2018
 */

#ifndef __READER_H__
#define __PHONE_FORWARD_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/**
 *  Skrócona nazwa na strukturę przechowującą bazę przekierowań.
 */
typedef struct DatabaseStruct Database;

/**
 * Struktura przechowująca bazę przekierowań.
 */
struct DatabaseStruct;

/** @brief Tworzy nową strukturę.
 * Tworzy nową strukturę bazy przekierowań.
 * @param[in] name – wskaźnik napis reprezentujący identyfikator bazy
 * @return Wskaźnik na utworzoną strukturę lub NULL, gdy nie udało się
 *         zaalokować pamięci.
 */
Database * DatabaseNew(char *name);

/** @brief Usuwa strukturę.
 * Usuwa strukturę wskazywaną przez @p db. Nic nie robi, jeśli wskaźnik ten ma
 * wartość NULL.
 * @param[in] db – wskaźnik na usuwaną strukturę.
 */
void DatabaseDelete(Database *db);

#endif /* __READER_H__ */
