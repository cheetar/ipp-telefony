#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "phone_forward.h"
#include "database.h"
#include "reader.h"
#include "utils.h"

#define DATABASES_INIT_SIZE 100

bool isEqual(char *s1, char *s2);

Buffer *b;
Database* active_db;

Database** dbs;
int dbs_size;
int dbs_count;

static void freeMemory(){
  BufferDelete(b);
  for(int i=0; i < dbs_size; i++){
    DatabaseDelete(dbs[i]);
  }
}

static void freeMemoryAndExit(int exit_code){
  freeMemory();
  exit(exit_code);
}

static void initialize(){
  Buffer *b = BufferNew();
  if(b == NULL){
    freeMemoryAndExit(1);
  }

  dbs_size = DATABASES_INIT_SIZE;
  dbs_count = 0;
  active_db = NULL;
  for(int i=0; i < DATABASES_INIT_SIZE; i++){
    dbs[i] = NULL;
  }
}

static void resizeDatabases(){
  Database *new_dbs[dbs_size * 2];

  for(int i=0; i < dbs_size; i++){
    new_dbs[i] = dbs[i];
  }
  for(int i=dbs_size; i < dbs_size * 2; i++){
    new_dbs[i] = NULL;
  }

  dbs = new_dbs;
}

static bool new_database(char *identifier){
  for(int i=0; i < dbs_size; i++){
    if(dbs[i] != NULL){
      if(isEqual(identifier, dbs[i]->name)){
        active_db = dbs[i];
        return true;
      }
    }
  }

  // If no database with given name was found create a new one.
  if(dbs_count == dbs_size){
    resizeDatabases();
  }
  for(int i=0; i < dbs_size; i++){
    if(dbs[i] == NULL){
      dbs[i] = DatabaseNew(identifier);
      if(dbs[i] == NULL){
        return false;
      }
      return true;
    }
  }

  return false;
}

static bool del_database(char *identifier){
  for(int i=0; i < dbs_size; i++){
    if(dbs[i] != NULL){
      if(isEqual(identifier, dbs[i]->name)){
        // When deleting current active database.
        if(dbs[i] == active_db){
          active_db = NULL;
        }

        DatabaseDelete(dbs[i]);
        dbs[i] = NULL;
        dbs_count--;
        return true;
      }
    }
  }

  return false;
}

static bool del_number(char *num){
  if(active_db != NULL){
    phfwdRemove(active_db->forwards, num);
    return true;
  }

  return false;
}

static bool add_redirect(char *num1, char *num2){
  if(active_db != NULL){
    phfwdAdd(active_db->forwards, num1, num2);
    return true;
  }

  return false;
}

static bool print_from_redirect(char *num){
  if(active_db != NULL){
    Numbers *numbers = phfwdGet(active_db->forwards, num);
    if(numbers == NULL){
      return false;
    }
    const char *number = phnumGet(numbers, 0);
    if(number == NULL){
      return false;
    }
    printf("%s\n", number);

    phnumDelete(numbers);
    return true;
  }

  return false;
}

static bool print_to_redirect(char *num){
  if(active_db != NULL){
    Numbers *numbers = phfwdReverse(active_db->forwards, num);
    if(numbers == NULL){
      return false;
    }
    const char *number;
    int i = 0;
    while((number = phnumGet(numbers, i)) != NULL){
      printf("%s\n", number);
      i++;
    }

    phnumDelete(numbers);
    return true;
  }

  return false;
}

// return true if successfully executed, false otherwise.
static bool executeCommand(Command *cmd){
  if(cmd->operator == 'N'){
    assert(cmd->second_arg == NULL);

    if(!new_database(cmd->first_arg)){
      fprintf(stderr, "ERROR NEW %lld\n", cmd->operator_place);
      return false;
    }
  }
  else if(cmd->operator == 'D'){
    assert(cmd->first_arg != NULL);
    assert(cmd->second_arg == NULL);

    // Delete database.
    if(isalpha(cmd->first_arg[0])){
      if(!del_database(cmd->first_arg)){
        fprintf(stderr, "ERROR DEL %lld\n", cmd->operator_place);
        return false;
      }
    }
    // Delete redirect.
    else if(isDigit(cmd->first_arg[0])){
      if(!del_number(cmd->first_arg)){
        fprintf(stderr, "ERROR DEL %lld\n", cmd->operator_place);
        return false;
      }
    }
    else{
      // Never should get there.
      assert(0 == 1);
    }
  }
  else if(cmd->operator == '?'){
    // number ?.
    if(cmd->first_arg != NULL && cmd->second_arg == NULL){
      if(!print_from_redirect(cmd->first_arg)){
        fprintf(stderr, "ERROR ? %lld\n", cmd->operator_place);
        return false;
      }
    }
    // ? number.
    else if(cmd->first_arg == NULL && cmd->second_arg != NULL){
      if(!print_to_redirect(cmd->first_arg)){
        fprintf(stderr, "ERROR ? %lld\n", cmd->operator_place);
        return false;
      }
    }
    else{
      // Never should get there.
      assert(0 == 1);
    }
  }
  else if(cmd->operator == '>'){
    assert(cmd->first_arg != NULL);
    assert(cmd->second_arg != NULL);

    if(!add_redirect(cmd->first_arg, cmd->second_arg)){
      fprintf(stderr, "ERROR > %lld\n", cmd->operator_place);
      return false;
    }
  }
  else{
    // Never should get there.
    assert(0 == 1);
  }

  return true;
}

int main() {

  initialize();

  Command *cmd;

  while((cmd = readCommand(b)) != NULL){
    printf("%lld\n", cmd->operator_place);

    if(!executeCommand(cmd)){
      freeMemoryAndExit(1);
    }
    CommandDelete(cmd);
  }

  freeMemory();
  return 0;
}
